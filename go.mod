module codeberg.org/vlbeaudoin/upcheck

go 1.22

require (
	github.com/prometheus-community/pro-bing v0.3.0
	gopkg.in/yaml.v3 v3.0.1
)

require (
	github.com/google/uuid v1.6.0 // indirect
	golang.org/x/net v0.21.0 // indirect
	golang.org/x/sync v0.6.0 // indirect
	golang.org/x/sys v0.17.0 // indirect
)
