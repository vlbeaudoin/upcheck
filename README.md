## upcheck

Small utility to ping a list of hostnames and/or IPs and return the result.

### usage

`upcheck DESTINATION...`
