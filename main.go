package main

import (
	"bytes"
	"flag"
	"fmt"
	"io"
	"log"
	"os"

	probing "github.com/prometheus-community/pro-bing"
	"gopkg.in/yaml.v3"
)

var destinations map[string]any

func init() {
	destinations = make(map[string]any)
}

func main() {
	flag.Parse()

	if err := run(os.Stdout, flag.Args()...); err != nil {
		log.Fatal(err)
	}
}

func run(w io.Writer, args ...string) error {
	switch len(args) {
	case 0:
		return fmt.Errorf("upcheck requires at least one destination hostname/ip provided as space-separated arguments")
	default:
		for _, destination := range args {
			//TODO sanitize destination

			pinger, err := probing.NewPinger(destination)
			if err != nil {
				return err
			}

			if pinger == nil {
				return fmt.Errorf("probing.NewPinger returned nil pointer")
			}

			pinger.Count = 3

			if err := pinger.Run(); err != nil {
				return err
			}

			stats := pinger.Statistics()
			if stats == nil {
				return fmt.Errorf("pinger.Statistics returned nil pointer")
			}

			var status string

			switch stats.PacketsRecv {
			case 0:
				status = "down"
			case stats.PacketsSent:
				status = "up"
			default:
				status = "partial up"
			}

			destinations[stats.IPAddr.IP.String()] = map[string]any{
				"as":     destination,
				"status": status,
			}

		}

		var buf bytes.Buffer

		if err := yaml.NewEncoder(&buf).Encode(destinations); err != nil {
			return err
		}

		fmt.Fprint(w, buf.String())
	}

	return nil
}
